<?php


namespace App\Libraries;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;


class WebApiResponse
{
    public static function error(int $status_code, array $error_details, string $message)
    {
        $responseData = [
            'status' => 'error',
            'message' => $message,
            'code' => $status_code,
            'errors' => $error_details

        ];
        return response()->json($responseData, $status_code, ['Content-Type' => 'text/json'], JSON_UNESCAPED_UNICODE);
    }

    public static function success(int $status_code, array $items, string $message)
    {

        $responseData = [
            'status' => 'success',
            'message' => $message,
            'code' => $status_code,
            'data' => $items

        ];
//        dd($responseData);

        return response()->json($responseData, $status_code, ['Content-Type' => 'application/json'], JSON_UNESCAPED_UNICODE);
    }
    public static function validationError(Validator $validator, Request $request)
    {

        $items = [];
        $errors = $validator->errors()->toArray();

        foreach ($errors as $index => $error) {
            $items[] = [
                'field' => $index,
                'value' => $request[$index],
                'message' => $errors[$index],
            ];
        }

        return self::error(
            400,
            $items,
            'Validation Error'
        );
    }

}
