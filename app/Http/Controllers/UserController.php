<?php

namespace App\Http\Controllers;

use App\Libraries\WebApiResponse;
use App\User;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $WebApiResponse;

    public function index()
    {


    }

    public function create(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'country' => 'required',
                'divission_state' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'password' => 'required',

            ]);

            if ($validator->fails()) {

                return redirect()->back();
            }

            if ($request) {
                $name = $request->name;
                $password = $request->password;
                $userData = [
                    'name' => $name,
                    'email' => $request->email,
                    'country' => $request->country,
                    'divission_state' => $request->divission_state,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'password' => Hash::make($password),

                ];

//                dd($request->all());
                $test = User::create($userData);
//                return redirect()->back();
                $status = 200;
                $data = $test->toArray();
                $message = "successful";
                return webApiResponse::success($status, $data, $message);


            } else {
                $status = 404;
                $data = [];
                $message = "Failed!!";
                return WebApiResponse::error($status, $data, $message);
            }


        } catch (\Throwable $th) {
            $status = 404;
            $data = [];
            $message = "Process failed, Please try again";
            return WebApiResponse::error($status, $data, $message);

        }


    }

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];
        // dd($credentials);
        if (auth()->attempt($request->all())) {

            $user = Auth::user();
            $user = $user->toArray();

            return WebApiResponse::success(200, $user, 'Login Success');

        } else {
            return WebApiResponse::error(400, $errors = [], 'Login Failed');
        }
    }


}
